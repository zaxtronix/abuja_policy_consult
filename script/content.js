
/**  Abuja policy Consulting
 *
 *  This script creates the behaviour of the chapters in the site
 *
 * **/


$(document).ready(function(){

    // creating an  array for all the chapters to be displayed
    var navArray = ['#C1','#C2','#C3','#C4','#C5','#C6','#C7','#C8','#C9'];

    // declaring an index variable for looping
    var index;

    // a function looping over the array to hide all the chapters
    function hideDisplay() {

    for (index = 0; index <= navArray.length; index++)
    $(navArray[index]).hide(1000).delay(1000);
    }

    // a function to hide all chapters and then displaying the chapter called
    function showDisplay() {

    var chapter;
    $(".navigation").click(function () {

    hideDisplay();
    chapter = $(this).attr("href");
    $(chapter).fadeIn(2000)
    });
    }
    hideDisplay();
    showDisplay();
    });

